package amrit.com.myapplication;

import android.content.pm.ActivityInfo;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class NotificationDetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notification_details);

        ActionBar supportActionBar = getSupportActionBar();

        if (supportActionBar != null)
            supportActionBar.hide();

        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.containsKey("POSITION")) {
            int position = extras.getInt("POSITION");
            Notifications notification = NotificationActivity.mNotificationses.get(position);

            ((TextView) findViewById(R.id.text_notification_name)).setText(notification.mNotificationName);

            ((TextView) findViewById(R.id.text_notification_date)).setText(notification.mDate);
        }
    }
}
