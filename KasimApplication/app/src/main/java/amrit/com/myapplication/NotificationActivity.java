package amrit.com.myapplication;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;

public class NotificationActivity extends AppCompatActivity {

    RecyclerView mRecyclerView;
    TextView mTextViewEmptyMessage;
    ProgressBar mProgressBarLoading;
    LinearLayout mLinearLayoutLoading;
    public static ArrayList<Notifications> mNotificationses;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        ActionBar supportActionBar = getSupportActionBar();

        if (supportActionBar != null) {
            supportActionBar.hide();
        }

        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view_notifications);
        mTextViewEmptyMessage = (TextView) findViewById(R.id.empty_view);
        mProgressBarLoading = (ProgressBar) findViewById(R.id.progress_download);
        mLinearLayoutLoading = (LinearLayout) findViewById(R.id.loading_layout);

        mRecyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this, 1);
        mRecyclerView.setLayoutManager(layoutManager);

        mNotificationses = new ArrayList<>();
        Notifications notifications = new Notifications();
        notifications.mNotificationName = "Dish of the day";
        notifications.mDate = "20-Feb-2016, 04.42 AM";
        mNotificationses.add(notifications);

        Notifications notifications1 = new Notifications();
        notifications1.mNotificationName = "Flat for sale";
        notifications1.mDate = "26-Feb-2016, 04.42 PM";
        mNotificationses.add(notifications1);


        Notifications notification = new Notifications();
        notification.mNotificationName = "Message From opal group";
        notification.mDate = "20-MAR-2016, 04.42 AM";
        mNotificationses.add(notification);

        mNotificationses.addAll(mNotificationses);

        mRecyclerView.setAdapter(new NotificationsAdapter(this));

//        ViewPager mPager = (ViewPager) findViewById(R.id.view_pager);
//        ScreenSlidePagerAdapter screenSlidePagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
//        mPager.setAdapter(screenSlidePagerAdapter);
//
//        mPager.setPageTransformer(true, new ZoomOutPageTransformer());
    }

//    @Override
//    public void onBackPressed() {
//        if (mPager.getCurrentItem() == 0) {
//            // If the user is currently looking at the first step, allow the system to handle the
//            // Back button. This calls finish() on this activity and pops the back stack.
//            super.onBackPressed();
//        } else {
//            // Otherwise, select the previous step.
//            mPager.setCurrentItem(mPager.getCurrentItem() - 1);
//        }
//    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case android.R.id.home:
//                finish();
//                break;
//        }
//        return true;
//    }

//    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
//        public ScreenSlidePagerAdapter(FragmentManager fm) {
//            super(fm);
//        }
//
//        @Override
//        public Fragment getItem(int position) {
//            return new ScreenSlidePageFragment();
//        }
//
//        @Override
//        public int getCount() {
//            return NUM_PAGES;
//        }
//    }
}
